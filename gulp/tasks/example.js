var gulp = require('gulp');
var config = require('../config')
var handleErrors = require('../util/handleErrors');

gulp.task('example', function() {
  return gulp.src(config.example.path + config.example.files)
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp));
});
