var gulp   = require('gulp');
var clean  = require('gulp-clean');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

// Clean the temporary folder
gulp.task('clean-tmp', function () {
  return gulp.src(config.env.tmp, {read: true})
    .on('error', handleErrors)
    .pipe(clean());
});

// Clean the build folder
gulp.task('clean-build', function () {
  return gulp.src(config.env.dst, {read: true})
    .on('error', handleErrors)
    .pipe(clean());
});

// Clean the CSS Folder
gulp.task('clean-css', function () {
  return gulp.src([config.env.tmp + config.css.path + '/**/_*.css'], {read: true})
    .on('error', handleErrors)
    .pipe(clean());
});