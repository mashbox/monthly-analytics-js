/* Notes:
   - gulp/tasks/browserify.js handles js recompiling with watchify
   - gulp/tasks/browserSync.js watches and reloads compiled files
*/

var gulp    = require('gulp');
var browserSync = require('browser-sync');
var config  = require('../config');

gulp.task('watch', function() {
  gulp.watch(config.example.path + config.example.files, ['example', browserSync.reload]);
});
